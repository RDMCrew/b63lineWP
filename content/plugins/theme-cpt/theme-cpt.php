<?php
/**
 * Plugin Name: Baldwin Custom Post Types
 * Plugin URI: http://baldwincreative.com/
 * Description: All of the Custom Post Types capsulated into a plugin for the Baldwin Creative website.
 * Author: Todd Miller
 * Version: 1.0.0
 * Author URI: http://rainydaymedia.net
 *
 * @package rdmgumby
 * @version 1.0
 */

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * add the actions for creating the cpt
 */
function theme_custom_post_types() {
    add_action( 'init', 'baldwin_cpt_business_options' );
    add_action( 'init', 'baldwin_cpt_home_sections' );
    add_action( 'init', 'baldwin_cpt_color_options' );
    add_action( 'init', 'baldwin_cpt_staff' );
    add_action( 'init', 'baldwin_cpt_collaborators' );
    add_action( 'init', 'baldwin_cpt_case_studies' );
}

/**
 * Create the labels array.
 *
 * @param string $singular - singular post label
 * @param string $plural (optional) - plural post label
 */
function build_post_type_labels( $singular, $plural = null )
{
    // if no plural given, just plop an 's' on the end
    if ($plural === null)
        $plural = $singular.'s';

    $labels = array(
        'name'               => __( $plural, 'rdmgumby'),
        'singular_name'      => __( $singular, 'rdmgumby'),
        'menu_name'          => __( $plural, 'rdmgumby'),
        'name_admin_bar'     => __( $singular, 'rdmgumby'),
        'add_new'            => __( 'Add New '.$singular, 'rdmgumby'),
        'add_new_item'       => __( 'Add New '.$singular, 'rdmgumby'),
        'new_item'           => __( 'New '.$singular, 'rdmgumby'),
        'edit_item'          => __( 'Edit '.$singular, 'rdmgumby'),
        'view_item'          => __( 'View '.$singular, 'rdmgumby'),
        'all_items'          => __( 'All '.$plural, 'rdmgumby'),
        'search_items'       => __( 'Search '.$plural, 'rdmgumby'),
        'parent_item_colon'  => __( 'Parent '.$singular.':', 'rdmgumby'),
        'not_found'          => __( 'No '.$plural.' found.', 'rdmgumby'),
        'not_found_in_trash' => __( 'No '.$plural.' found in Trash.', 'rdmgumby')
    );

    return $labels;
}

/**
 * Creates an options page for info about the business
 */
function baldwin_cpt_business_options()
{
    if ( function_exists( 'acf_add_options_page' ) ) {
        $page = acf_add_options_page(array(
            'page_title' => 'Business Info',
            'menu_slug'  => 'business-info',
            'capability' => 'manage_options',
            'position'   => '56',
            'icon_url'   => 'dashicons-store',
            'redirect'   => false
        ));
    }
}

/**
 * create the home sections cpt
 */
function baldwin_cpt_home_sections()
{
    $postLabels = build_post_type_labels( 'Home Section' );
    $postArgs = array(
        'label'               => 'home_sections',
        'description'         => 'Single Section Entries',
        'labels'              => $postLabels,
        'supports'            => array( 'title', 'revisions' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 30,
        'menu_icon'           => 'dashicons-editor-justify',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post'
    );

    register_post_type( 'home_sections', $postArgs );
}

/**
 * create the colors!
 */
function baldwin_cpt_color_options()
{
    if ( function_exists( 'acf_add_options_page' ) ) {
        $page = acf_add_options_page(array(
            'page_title' => 'Colors',
            'menu_slug'  => 'theme-colors',
            'capability' => 'manage_colors',
            'position'   => '34',
            'icon_url'   => 'dashicons-art',
            'redirect'   => false
        ));
    }
}

/**
 * create the staff!
 */
function baldwin_cpt_staff()
{
    $postLabels = build_post_type_labels( 'Staff Member' );
    $postArgs = array(
        'label'               => 'staff',
        'description'         => 'Staff Member Profiles',
        'labels'              => $postLabels,
        'supports'            => array( 'title', 'content', 'revisions' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 32,
        'menu_icon'           => 'dashicons-businessman',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true ,
        'capability_type'     => 'post'
    );

    register_post_type( 'staff', $postArgs );
}

/**
 * create the collaborators!
 */
function baldwin_cpt_collaborators()
{
    $postLabels = build_post_type_labels( 'Collaborator' );
    $postArgs = array(
        'label'               => 'collaborators',
        'description'         => 'Collaborator Profiles',
        'labels'              => $postLabels,
        'supports'            => array( 'title', 'revisions' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 33,
        'menu_icon'           => 'dashicons-groups',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true ,
        'capability_type'     => 'post'
    );

    register_post_type( 'collaborators', $postArgs );
}

/**
 * create the case studies!
 */
function baldwin_cpt_case_studies()
{
    $postLabels = build_post_type_labels( 'Case Study', 'Case Studies' );
    $postArgs = array(
        'label'               => 'casestudies',
        'description'         => 'Case Study Posts',
        'labels'              => $postLabels,
        'supports'            => array( 'title', 'revisions', 'content', 'thumbnail' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 31,
        'menu_icon'           => 'dashicons-portfolio',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post'
    );

    register_post_type( 'casestudies', $postArgs );
}
