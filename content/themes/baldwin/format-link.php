<?php
/**
 * Link Post Format template
 */
require_once( get_template_directory() . '/inc/OpenGraph.php' );
$graph = OpenGraph::fetch( get_field( 'external_url' ) );

if ( ! empty( $graph ) ) {
    $og_title = ( $graph->__isset( 'title' ) ) ? $graph->__get( 'title' ) : '';
    $og_desc  = ( $graph->__isset( 'description' ) ) ? $graph->__get( 'description' ) : '';
    $og_image = ( $graph->__isset( 'image' ) ) ? $graph->__get( 'image' ) : '';
}

$page_id = get_option( 'page_for_posts' );
?>

<h1 class="no-pad"><?php the_title(); ?></h1>
<hr style="border-color: <?php __the_field( 'color_theme', 'esc_attr', $page_id ); ?>;" />
<?php the_content(); ?>
<hr style="border-color: <?php __the_field( 'color_theme', 'esc_attr', $page_id ); ?>;" />
<div class="row open-graph-data">
    <div class="ten columns">
        <h5><?php echo $og_title; ?></h5>
        <p><?php echo $og_desc; ?></p>
        <p><a href="<?php __the_field( 'external_url', 'esc_url' ); ?>" target="_blank"><?php __the_field( 'external_url', 'esc_url' ); ?></a></p>
    </div>
    <div class="five columns push_one">
        <img src="<?php echo $og_image; ?>" />
    </div>
</div>
