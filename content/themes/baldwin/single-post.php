<?php
/**
 * @package rdmgumby
 */
$cats = get_the_category();

get_header(); ?>

<div class="page blog bg-white">
    <div class="l-section hero dark typography">
        <?php the_post_thumbnail(); ?>
    </div>

    <div class="l-section l-padded-tiny bg-black dark typography">
        <div class="row">
            <div class="fourteen columns centered">
                <h5 class="uppercase">
                    <span><img class="market-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-blog.svg" /></span>
                    <span class="market-copy"><?php echo $cats[0]->name; ?></span>
                </h5>
            </div>
        </div>
    </div>

    <div class="nav-arrows clearfix">
        <?php previous_post_link( '%link', '<div class="nav-arrow left"><i class="icon-arrow-left"></i></div>' ); ?>
        <?php next_post_link( '%link', '<div class="nav-arrow right"><i class="icon-arrow-right"></i></div>' ); ?>
    </div>

    <section class="l-padded-small light typography">
        <article>
            <?php
                $format = get_post_format() ? : 'standard';
                get_template_part( 'format', $format );
            ?>
        </article>
    </section>

</div>

<?php
    get_footer();
