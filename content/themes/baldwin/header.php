<?php
/**
 * The Header for the theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package rdmgumby
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js sixteen colgrid">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="author" href="<?php echo get_template_directory_uri(); ?>/inc/humans.txt">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js" charset="utf-8"></script>
    <!-- GOOGLE ANALYTICS -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-64695709-1', 'auto');
        ga('send', 'pageview');
    </script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="filters">
    <defs>
        <filter id="blur">
            <feGaussianBlur in="SourceGraphic" stdDeviation="0,0" />
        </filter>
    </defs>
</svg>

<div id="mobile-menu-modal" class="mobile-menu-modal bg-bg-gray dark typography">
    <div class="mobile-menu-logo text-right">
        <?php the_field( 'full_logo_svg', 'options' ); ?>
    </div>
    <div class="l-v-margin mobile-menu">
        <?php
            $args = array(
                'theme_location'  => 'mobile',
                'container'       => false,
                'menu_class'      => 'menu-items text-center',
                'before'          => '<h4 class="smaller light">',
                'after'           => '</h4>'
            );

            wp_nav_menu( $args );
        ?>
    </div>
</div>

<div id="page" class="site">

	<header id="masthead" class="site-header" role="banner">
        <div class="masthead">
            <div class="logo">
                <a href="<?php echo home_url(); ?>">
                    <?php the_field( 'simple_logo_svg', 'options' ); ?>
                </a>

            </div>
            <div class="menu">
                <?php wp_nav_menu(array('theme_location' => 'bubble', 'container' => '', 'menu_class' => '')); ?>
            </div>
        </div>

        <div class="l-mobile mobile-masthead">
            <div class="background"></div>

            <div class="l-ignore-overlay">
                <a href="<?php echo home_url(); ?>" class="mobile-logo">
                    <?php the_field( 'mobile_logo_svg', 'options' ); ?>
                </a>

                <div class="mobile-quick-menu">
                    <a id="mobile-menu-button" href="#" class="menu-button-wrapper">
                    <div class="menu-button">
                        <div class="line one"><span></span></div>
                        <div class="line two"><span></span></div>
                    </div>
                    </a>
                </div>
            </div>
        </div>

<!--         <div class="hero-logo">
            <a href="<//php echo home_url(); ?>">
                <//php the_field( 'simple_logo_svg', 'options' ); ?>
            </a>
        </div> -->

<!-- Old MENU 06-27-2016-->
<!--     <\\php if ( !is_front_page() ) : ?>
        <div class="quick-menu">
            <a href="#" class="menu-button-wrapper toggle" gumby-classname="is-active" gumby-trigger=".floating-nav">
            <div class="menu-button">
                <div class="line one"><span></span></div>
                <div class="line two"><span></span></div>
            </div>
            </a>
            <\\php baldwin_output_floating_nav(); ?>
        </div>
    <\\php endif; ?> -->

	</header><!-- #masthead -->

	<div id="content" class="site-content">

<?php
    add_action( 'wp_footer', 'baldwin_init_mobile_menu', 99 );
    function baldwin_init_mobile_menu() {
        echo '<script>mobileMenu.init();</script>';
    }
