<?php
/**
 * @package rdmgumby
 */
get_header(); ?>

<div class="page">
    <div class="l-full-section hero dark typography <?php echo $hero_class; ?>">
        <div id="the-bg" class="background"><div class="gray overlay"></div></div>
        <div class="l-ignore-overlay">

            <div class="row copy">
                <div class="eleven columns centered text-center">
                    <h6 class="light spaced uppercase">Page Not Found</h6>
                    <h1 class="no-pad">You're off track.</h1>
                    <hr class="center border-yellow" />
                    <p>Oops! It appears you're lost. Let's head back to that last junction and try a different route.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
rdmgumby_output_responsive_backgrounds();
get_footer();
