<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package rdmgumby
 */
get_header(); ?>

<div class="l-full-section hero dark typography" data-bubble="0">
    <video autoplay loop poster="<?php echo get_template_directory_uri(); ?>/assets/video/poster.jpg">
        <source src="<?php echo get_template_directory_uri(); ?>/assets/video/baldwin_header.mp4" type="video/mp4" />
    </video>
    <div id="video-fallback" class="background"></div>
    <div id="video-overlay" class="gray overlay"></div>

    <div class="l-ignore-overlay">
        <?php baldwin_output_floating_nav( null, true ); ?>
        <svg id="hero-track" class="track-piece"></svg>

        <div class="row copy">
            <div class="nine columns push_two">
                <h1 class="no-pad">Get On Track.</h1>
                <hr class="border-yellow" />
                <p>They're out there - your customers, clients and constituents - and they're looking for a reason to engage. But driving them to action requires building awareness, understanding and relevancy.</p>
                <h4 class="light text-yellow">Are you on the right track?</h4>
            </div>
        </div>
    </div>
</div>

<div class="l-full-section about dark typography waypoint" data-target="about" data-bubble="1">
    <div class="background"><div class="gray overlay"></div><div class="gradient overlay"></div></div>
    <div class="l-ignore-overlay">
        <svg id="about-track" class="track-piece"></svg>

        <div class="row copy">
            <div class="ten columns push_two">
                <h1 class="no-pad">Move Markets.</h1>
                <hr class="border-green" />
                <p>As experts in audience engagement, we move markets by placing people and relationships first. We make you look great and build lasting relationships along the way.</p>
            </div>
        </div>
    </div>
    <div class="row l-v-margin xlarge call-to-action">
        <div class="ten columns push_two">
            <div class="green button standard mobile-text-center">
                <a href="about-us/">Who We Are</a>
            </div>
        </div>
    </div>
</div>

<div class="l-full-section services dark typography waypoint" data-target="services" data-bubble="2">
    <div class="background"><div class="gray overlay"></div><div class="gradient overlay"></div></div>
    <div class="l-ignore-overlay">
        <svg id="services-track" class="track-piece"></svg>

        <div class="row copy">
            <div class="ten columns push_two">
                <h1 class="no-pad">Realize Success.</h1>
                <hr class="border-red" />
                <p>Delivering superior strategy and performance requires listening and understanding. Intent listening and smart questions are the foundation for all of our services - and where your success begins.</p>
            </div>
        </div>
    </div>
    <div class="row l-v-margin xlarge call-to-action">
        <div class="ten columns push_two">
            <div class="red button standard mobile-text-center">
                <a href="services/">What We Do</a>
            </div>
        </div>
    </div>
</div>

<div class="l-full-section work dark typography waypoint" data-target="work" data-bubble="3">
    <div class="background"><div class="gray overlay"></div><div class="gradient overlay"></div></div>
    <div class="l-ignore-overlay">
        <svg id="work-track" class="track-piece"></svg>

        <div class="row copy">
            <div class="ten columns push_two">
                <h1 class="no-pad">Pause to Celebrate.</h1>
                <hr class="border-purple" />
                <p>With exceptional industry experience in the Healthcare, Educational and Non-Profit sectors, Baldwin Creative also connects all aspects of marketing and business development for B2B, B2C and government organizations.</p>
            </div>
        </div>
    </div>
    <div class="row l-v-margin xlarge call-to-action">
        <div class="ten columns push_two">
            <div class="purple button standard mobile-text-center">
                <a href="case-studies/">Case Studies</a>
            </div>
        </div>
    </div>
</div>

<div class="l-full-section blog dark typography waypoint" data-target="blog" data-bubble="4">
    <div class="background"><div class="gray overlay"></div><div class="gradient overlay"></div></div>
    <div class="l-ignore-overlay">
        <svg id="blog-track" class="track-piece"></svg>

        <div class="row copy">
            <div class="ten columns push_two">
                <h1 class="no-pad">... and get smarter.</h1>
                <hr class="border-yellow" />
                <p>It's not rocket science, but successful audience engagement does take strategic thinking. Follow us as we share some wisdom, dispel a few myths and have a little fun along the way.</p>
            </div>
        </div>
    </div>
    <div class="row l-v-margin xlarge call-to-action">
        <div class="ten columns push_two">
            <div class="yellow button standard mobile-text-center">
                <a href="blog/">Follow Us</a>
            </div>
        </div>
    </div>
</div>

<div class="l-full-section contact dark typography waypoint" data-target="contact" data-bubble="5">
    <div class="background"><div class="gray overlay"></div><div class="gradient overlay"></div></div>
    <div class="l-ignore-overlay">
        <svg id="contact-track" class="track-piece"></svg>

        <div class="row copy">
            <div class="twelve columns push_two">
                <h1 class="no-pad">Start Here.</h1>
                <hr class="border-brown" />
                <div class="row">
                    <?php baldwin_output_business_info( 'five columns business-info internal' ); ?>
                    <div class="six columns business-info external">
                        <?php
                            baldwin_output_business_logos();
                            baldwin_output_social_links();
                        ?>
                        <p class="l-v-margin tiny copyright">© 2015 Baldwin Creative & Co. | All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    add_action( 'wp_footer', 'baldwin_build_tracks', 99 );
    function baldwin_build_tracks() {
        echo '<script>fixedBackgrounds.init();</script>';
        echo '<script>trackBuilder.init();</script>';
        echo '<script>floatingNav.init();</script>';
    }
?>

<?php get_footer();
