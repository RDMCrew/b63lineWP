<?php
/**
 * Widget API: Baldwin_Widget_Quote_Box class
 */

/**
 * Core class used to implement a Text widget.
 *
 * @see WP_Widget
 */
class Baldwin_Widget_Service_Box extends WP_Widget {

	/**
	 * Sets up a new Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'baldwin_widget_service_box', 'description' => __('Service box for the Services page of the Baldwin site.'));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('baldwin_service_box', __('Baldwin Service Box'), $widget_ops, $control_ops);
	}

	/**
	 * Outputs the content for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Text widget instance.
	 */
	public function widget( $args, $instance ) {

        $widget_title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$widget_text  = ! empty( $instance['text'] ) ? $instance['text'] : '';
        $widget_bullets = ! empty( $instance['bullets'] ) ? $instance['bullets'] : '';

        $bullets = explode(PHP_EOL, $widget_bullets);
        $color_field = get_field_object( 'color_theme' );
        $color = get_field( 'color_theme' );
        $color_label = strtolower( $color_field['choices'][$color] );

		/**
		 * Filter the content of the Text widget.
		 *
		 * @since 2.3.0
		 * @since 4.4.0 Added the `$this` parameter.
		 *
		 * @param string         $widget_text The widget content.
		 * @param array          $instance    Array of settings for the current widget.
		 * @param WP_Widget_Text $this        Current Text widget instance.
		 */
		$text = apply_filters( 'widget_text', $widget_text, $instance, $this );

		echo $args['before_widget'];
		?>

        <div class="row light typography">
            <div class="fourteen columns centered l-padded">
                <h2 class="no-pad"><?php echo $widget_title; ?></h2>
                <hr style="border-color: <?php __the_field( 'color_theme' ); ?>;" />
                <div class="row">
                    <div class="twelve columns">
                        <?php echo !empty( $instance['filter'] ) ? wpautop( $text ) : $text; ?>
                        <div class="row">

                        <?php for ( $i = 0; $i < 3; $i++ ) : ?>
                            <div class="five columns">
                                <h6 class="regular bigger">
                                <?php for ( $j = 0; $j < 3; $j++ ) : ?>
                                    <?php
                                        if ( ! empty( $bullets[($i*3)+$j] ) )
                                            echo $bullets[($i*3)+$j];
                                    ?><br />
                                <?php endfor; ?>
                                </h6>
                            </div>
                        <?php endfor; ?>

                        </div>
                        <div class="l-v-margin xxlarge button standard mobile-text-center <?php echo $color_label; ?>">
                            <a href="<?php echo home_url(); ?>/case-studies">View Samples</a>
                        </div>
                    </div>
                    <div class="four columns text-center">
                        <img class="services-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-<?php echo strtolower( $widget_title ); ?>.svg" />
                    </div>
                </div>
            </div>
        </div>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		if ( current_user_can('unfiltered_html') )
			$instance['text'] =  $new_instance['text'];
		else
			$instance['text'] = wp_kses_post( stripslashes( $new_instance['text'] ) );

        $instance['title']   = $new_instance['title'];
		$instance['filter']  = ! empty( $new_instance['filter'] );
        $instance['bullets'] = wp_kses_post( stripslashes( $new_instance['bullets'] ) );
		return $instance;
	}

	/**
	 * Outputs the Text widget settings form.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '', 'bullets' => '' ) );
		$filter = isset( $instance['filter'] ) ? $instance['filter'] : 0;
		?>

        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="wide" type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Content:' ); ?></label>
		<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo esc_textarea( $instance['text'] ); ?></textarea></p>

		<p><input id="<?php echo $this->get_field_id('filter'); ?>" name="<?php echo $this->get_field_name('filter'); ?>" type="checkbox"<?php checked( $filter ); ?> />&nbsp;<label for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs'); ?></label></p>

        <p><label for="<?php echo $this->get_field_id( 'bullets' ); ?>"><?php _e( 'Bullets:' ); ?></label>
        <textarea class="widefat" rows="9" cols="20" id="<?php echo $this->get_field_id('bullets'); ?>" name="<?php echo $this->get_field_name('bullets'); ?>"><?php echo esc_textarea( $instance['bullets'] ); ?></textarea></p>
		<?php
	}
}
