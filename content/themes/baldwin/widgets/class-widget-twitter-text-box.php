<?php
/**
 * Widget API: Baldwin_Widget_Quote_Box class
 */

/**
 * Core class used to implement a Text widget.
 *
 * @see WP_Widget
 */
class Baldwin_Widget_Twitter_Text_Box extends WP_Widget {

	/**
	 * Sets up a new Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'baldwin_widget_twitter_text_box', 'description' => __('Arbitrary text or HTML, with a title, next to a twitter feed carousel, styled for the Baldwin site.'));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('baldwin_twitter_text_box', __('Baldwin Twitter Feed with Text Box'), $widget_ops, $control_ops);
	}

	/**
	 * Outputs the content for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Text widget instance.
	 */
	public function widget( $args, $instance ) {
        add_action( 'wp_footer', array( $this, 'baldwin_twitter_widget_init' ), 99 );

        $widget_title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$widget_text  = ! empty( $instance['text'] ) ? $instance['text'] : '';
        $widget_twitter = ! empty( $instance['twitter'] ) ? $instance['twitter'] : '';

		/**
		 * Filter the content of the Text widget.
		 *
		 * @since 2.3.0
		 * @since 4.4.0 Added the `$this` parameter.
		 *
		 * @param string         $widget_text The widget content.
		 * @param array          $instance    Array of settings for the current widget.
		 * @param WP_Widget_Text $this        Current Text widget instance.
		 */
		$text = apply_filters( 'widget_text', $widget_text, $instance, $this );

		echo $args['before_widget'];
        ?>
            <div class="l-half-section right bg-bg-gray"></div>
            <div class="row l-ignore-overlay">
                <div class="six columns push_one light typography l-padded-small">
                    <h2 class="no-pad"><?php echo $widget_title; ?></h2>
                    <hr class="border-green" />
                    <?php echo !empty( $instance['filter'] ) ? wpautop( $text ) : $text; ?>
                </div>

                <div class="six columns push_two dark typography">
                    <h4 class="l-v-margin text-center"><i class="icon-twitter"></i></h4>
                    <?php
                        $tweets = getTweets( $widget_twitter, 5 );
                    ?>
                    <div id="twitter-carousel" class="owl-carousel">
                    <?php foreach ( $tweets as $tweet ) : ?>
                        <div>
                            <p class="l-v-margin large text-center"><?php echo $tweet['text']; ?></p>
                            <p class="l-v-margin xlarge smaller italic text-center"><?php echo $widget_twitter; ?> | <?php echo rdmgumby_time_ago( strtotime( $tweet['created_at'] ) ); ?></p>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		if ( current_user_can('unfiltered_html') )
			$instance['text'] =  $new_instance['text'];
		else
			$instance['text'] = wp_kses_post( stripslashes( $new_instance['text'] ) );

        $instance['title']  = $new_instance['title'];
		$instance['filter'] = ! empty( $new_instance['filter'] );
        $instance['twitter']  = $new_instance['twitter'];
		return $instance;
	}

	/**
	 * Outputs the Text widget settings form.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '', 'twitter' => '' ) );
		$filter = isset( $instance['filter'] ) ? $instance['filter'] : 0;
		?>

        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="wide" type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Content:' ); ?></label>
		<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo esc_textarea( $instance['text'] ); ?></textarea></p>

		<p><input id="<?php echo $this->get_field_id('filter'); ?>" name="<?php echo $this->get_field_name('filter'); ?>" type="checkbox"<?php checked( $filter ); ?> />&nbsp;<label for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs'); ?></label></p>

        <p><label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php _e( 'Tweeter Account:' ); ?></label>
        <input class="wide" type="text" id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" value="<?php echo $instance['twitter']; ?>" /></p>
		<?php
	}

    /**
     *
     */
    public function baldwin_twitter_widget_init() {
        echo '<script>twitterCarousel.init();</script>';
    }
}
