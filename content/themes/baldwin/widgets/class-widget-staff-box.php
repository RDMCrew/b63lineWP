<?php
/**
 * Widget API: Baldwin_Widget_Quote_Box class
 */

/**
 * Core class used to implement a Text widget.
 *
 * @see WP_Widget
 */
class Baldwin_Widget_Staff_Box extends WP_Widget {

	/**
	 * Sets up a new Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'baldwin_widget_staff_box', 'description' => __('Gallery of the Staff Members custom post type, styled for the Baldwin site.'));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('baldwin_staff_box', __('Baldwin Staff Members Box'), $widget_ops, $control_ops);
	}

	/**
	 * Outputs the content for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Text widget instance.
	 */
	public function widget( $args, $instance ) {
        add_action( 'wp_footer', array( $this, 'baldwin_staff_widget_init' ), 99 );
        add_action( 'wp_footer', 'baldwin_output_team_bios' );

        $posts = array();

        $post_args = array(
            'post_type' => 'staff',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC'
        );

        $posts = get_posts( $post_args );

		echo $args['before_widget'];
		?>

            <div class="row">
                <div class="fourteen columns centered l-padded-small">
                    <ul class="team-list">

            <?php
                if ( $posts ) :
                    foreach ( $posts as $post ) :
                        setup_postdata( $post );

                        $bio_id = strtolower( str_replace( ' ', '', get_the_title( $post->ID ) ) );
            ?>

                    <li>
                        <a href="#" class="text-white team-bio-trigger" data-bio="#<?php echo $bio_id; ?>">
                        <div class="team-member toggle" gumby-classname="is-active" gumby-on="mouseover mouseout" style="background-image: url('<?php __the_field( 'picture', 'esc_url', $post->ID ); ?>');">
                            <div class="overlay" style="background: <?php __the_field( 'color_theme', 'esc_attr', $post->ID ); ?>;"></div>
                            <div class="l-ignore-overlay team-member-copy text-center">
                                <h5 class="name bigger bold uppercase no-pad"><?php echo get_the_title( $post->ID ); ?></h5>
                                <p class="position regular uppercase no-margin text-center"><?php __the_field( 'position', 'esc_html', $post->ID ); ?></p>
                            </div>
                        </div>
                        </a>
                    </li>

            <?php
                    endforeach;
                endif;
                wp_reset_postdata();
            ?>

                    </ul>
                </div>
            </div>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		return $instance;
	}

	/**
	 * Outputs the Text widget settings form.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
	}

    /**
     *
     */
    public function baldwin_staff_widget_init() {
        echo '<script>teamBios.init();</script>';
    }
}
