<?php
/**
 * Widget API: Baldwin_Widget_Quote_Box class
 */

/**
 * Core class used to implement a Text widget.
 *
 * @see WP_Widget
 */
class Baldwin_Widget_Call_To_Action_Box extends WP_Widget {

	/**
	 * Sets up a new Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'baldwin_widget_call_to_action_box', 'description' => __('Arbitrary large text or HTML, plus a button, styled for the Baldwin site.'));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('baldwin_call_to_action_box', __('Baldwin Call-to-Action Box'), $widget_ops, $control_ops);
	}

	/**
	 * Outputs the content for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Text widget instance.
	 */
	public function widget( $args, $instance ) {

		$widget_text = ! empty( $instance['text'] ) ? $instance['text'] : '';
        $widget_button_text = ! empty( $instance['button_text'] ) ? $instance['button_text'] : '';
        $widget_button_link = ! empty( $instance['button_link'] ) ? $instance['button_link'] : '';

        $color_field = get_field_object( 'color_theme' );
        $color = get_field( 'color_theme' );
        $color_label = strtolower( $color_field['choices'][$color] );

        $widget_text = preg_replace( '/<span>/', '<span class="text-'.$color_label.'">', $widget_text );

        if ( ! empty( $instance['button_use_option'] ) )
            $widget_button_link = get_field( $widget_button_link, 'options' );

		/**
		 * Filter the content of the Text widget.
		 *
		 * @since 2.3.0
		 * @since 4.4.0 Added the `$this` parameter.
		 *
		 * @param string         $widget_text The widget content.
		 * @param array          $instance    Array of settings for the current widget.
		 * @param WP_Widget_Text $this        Current Text widget instance.
		 */
		$text = apply_filters( 'widget_text', $widget_text, $instance, $this );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} ?>

            <div class="row light typography">
                <div class="fourteen columns centered text-center l-padded-small">
                    <h4 class="light uppercase"><?php echo $text; ?></h4>
                    <div class="l-v-margin larger button standard <?php echo $color_label; ?>">
                        <a href="<?php echo $widget_button_link; ?>" target="_blank"><?php echo $widget_button_text; ?></a>
                    </div>
                </div>
            </div>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		if ( current_user_can('unfiltered_html') )
			$instance['text'] =  $new_instance['text'];
		else
			$instance['text'] = wp_kses_post( stripslashes( $new_instance['text'] ) );

        $instance['button_text'] = wp_kses_post( stripslashes( $new_instance['button_text'] ) );
        $instance['button_link'] = wp_kses_post( stripslashes( $new_instance['button_link'] ) );
        $instance['button_use_option'] = ! empty( $new_instance['button_use_option']);
		return $instance;
	}

	/**
	 * Outputs the Text widget settings form.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'text' => '', 'button_text' => '', 'button_link' => '' ) );
        $button_use_option = isset( $instance['button_use_option'] ) ? $instance['button_use_option'] : 0;
		?>

		<p><label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Content:' ); ?></label>
		<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo esc_textarea( $instance['text'] ); ?></textarea></p>

        <p><label for="<?php echo $this->get_field_id( 'button_text' ); ?>"><?php _e( 'Button Text:' ); ?></label>
        <input class="wide" type="text" id="<?php echo $this->get_field_id( 'button_text' ); ?>" name="<?php echo $this->get_field_name( 'button_text' ); ?>" value="<?php echo $instance['button_text']; ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'button_link' ); ?>"><?php _e( 'Button Link:' ); ?></label>
        <input class="wide" type="text" id="<?php echo $this->get_field_id( 'button_link' ); ?>" name="<?php echo $this->get_field_name( 'button_link' ); ?>" value="<?php echo $instance['button_link']; ?>" /></p>

        <p><input id="<?php echo $this->get_field_id( 'button_use_option' ); ?>" name="<?php echo $this->get_field_name('button_use_option'); ?>" type="checkbox"<?php checked( $button_use_option ); ?> />&nbsp;<label for="<?php echo $this->get_field_id('button_use_option'); ?>"><?php _e('Interpret Link as Business Options Field'); ?></label></p>
		<?php
	}
}
