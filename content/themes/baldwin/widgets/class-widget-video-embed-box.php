<?php
/**
 * Widget API: Baldwin_Widget_Quote_Box class
 */

/**
 * Core class used to implement a Text widget.
 *
 * @see WP_Widget
 */
class Baldwin_Widget_Video_Embed_Box extends WP_Widget {

	/**
	 * Sets up a new Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'baldwin_widget_video_embed_box', 'description' => __('Fancy embedded video, styled for the Baldwin site.'));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('baldwin_video_embed_box', __('Baldwin Video Embed Box'), $widget_ops, $control_ops);
	}

	/**
	 * Outputs the content for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Text widget instance.
	 */
	public function widget( $args, $instance ) {

        $widget_video = ! empty( $instance['video'] ) ? $instance['video'] : '';

		echo $args['before_widget'];
		?>

            <div class="l-video-wrapper l-v-margin large">
                <iframe src="<?php echo $widget_video; ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
        $instance['video']  = $new_instance['video'];
		return $instance;
	}

	/**
	 * Outputs the Text widget settings form.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'video' => '' ) );
		?>

        <p><label for="<?php echo $this->get_field_id( 'video' ); ?>"><?php _e( 'Video URL:' ); ?></label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'video' ); ?>" name="<?php echo $this->get_field_name( 'video' ); ?>" value="<?php echo $instance['video']; ?>" /></p>
		<?php
	}
}
