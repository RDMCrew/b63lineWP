<?php
/**
 * Widget API: Baldwin_Widget_Quote_Box class
 */

/**
 * Core class used to implement a Text widget.
 *
 * @see WP_Widget
 */
class Baldwin_Widget_Collaborators_Box extends WP_Widget {

	/**
	 * Sets up a new Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'baldwin_widget_collaborators_box', 'description' => __('Gallery of the Collaborators custom post type, styled for the Baldwin site.'));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('baldwin_collaborators_box', __('Baldwin Collaborators Box'), $widget_ops, $control_ops);
	}

	/**
	 * Outputs the content for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Text widget instance.
	 */
	public function widget( $args, $instance ) {
        $posts = array();

        $post_args = array(
            'post_type' => 'collaborators',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC'
        );

        $posts = get_posts( $post_args );

		echo $args['before_widget'];
		?>

            <div class="row">
                <div class="fourteen columns centered l-padded-small">
                    <ul>

            <?php
                if ( $posts ) :
                    foreach ( $posts as $post ) :
                        setup_postdata( $post );
            ?>

                    <li>
                    <?php if ( ! empty( get_field( 'link', $post->ID ) ) ) : ?>
                        <a href="<?php __the_field( 'link', 'esc_url', $post->ID ); ?>" target="_blank">
                    <?php endif; ?>
                            <img src="<?php __the_field( 'logo', 'esc_url', $post->ID ); ?>" />
                    <?php if ( ! empty( get_field( 'link', $post->ID ) ) ) : ?>
                        </a>
                    <?php endif; ?>
                    </li>

            <?php
                    endforeach;
                endif;
                wp_reset_postdata();
            ?>

                    </ul>
                </div>
            </div>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		return $instance;
	}

	/**
	 * Outputs the Text widget settings form.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
	}
}
