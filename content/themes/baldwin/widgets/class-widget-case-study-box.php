<?php
/**
 * Widget API: Baldwin_Widget_Quote_Box class
 */

/**
 * Core class used to implement a Text widget.
 *
 * @see WP_Widget
 */
class Baldwin_Widget_Case_Study_Box extends WP_Widget {

	/**
	 * Sets up a new Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'baldwin_widget_case_study_box', 'description' => __('Case Study box for the Case Study listing, styled for the Baldwin site.'));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('baldwin_case_study_box', __('Baldwin Case Study Box'), $widget_ops, $control_ops);

        add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
	}

    /**
     * Upload the Javascripts for the media uploader
     */
    public function upload_scripts()
    {
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', get_template_directory_uri() . '/widgets/upload-media.js', array('jquery'));

        wp_enqueue_style('thickbox');
    }

	/**
	 * Outputs the content for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Text widget instance.
	 */
	public function widget( $args, $instance ) {

        $widget_title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$widget_tags  = ! empty( $instance['tags'] ) ? $instance['tags'] : '';
        $widget_slug  = ! empty( $instance['slug'] ) ? $instance['slug'] : '';
        $widget_image = ! empty( $instance['image'] ) ? $instance['image'] : '';
        $widget_logo = ! empty( $instance['logo'] ) ? $instance['logo'] : '';

        $color_field = get_field_object( 'color_theme' );
        $color = get_field( 'color_theme' );
        $color_label = strtolower( $color_field['choices'][$color] );

		echo $args['before_widget'];
        ?>

            <div class="l-section l-padded case-study-item dark typography">
                <div class="background" style="background-image: url('<?php echo esc_url( $widget_image ); ?>');"></div>
                <div class="gray overlay"></div>

                <div class="row l-ignore-overlay">
                    <div class="fourteen columns centered text-center">
                        <img src="<?php echo esc_url( $widget_logo ); ?>" />
                        <h2 class="l-v-margin short no-pad"><?php echo $widget_title; ?></h2>
                        <p class="l-v-margin short text-center"><?php echo $widget_tags; ?></p>
                        <div class="l-v-margin xlarge button standard <?php echo $color_label; ?>">
                            <a href="<?php echo home_url(); ?>/casestudies/<?php echo $widget_slug; ?>">View Case Study</a>
                        </div>
                    </div>
                </div>
            </div>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
        $instance['title']  = $new_instance['title'];
        $instance['tags']  = $new_instance['tags'];
        $instance['slug']  = $new_instance['slug'];
        $instance['image'] = $new_instance['image'];
        $instance['logo'] = $new_instance['logo'];
		return $instance;
	}

	/**
	 * Outputs the Text widget settings form.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'tags' => '', 'slug' => '', 'image' => '', 'logo' => '' ) );
		?>

        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'tags' ); ?>"><?php _e( 'Tags:' ); ?></label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'tags' ); ?>" name="<?php echo $this->get_field_name( 'tags' ); ?>" value="<?php echo $instance['tags']; ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'slug' ); ?>"><?php _e( 'Link / Slug:' ); ?></label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'slug' ); ?>" name="<?php echo $this->get_field_name( 'slug' ); ?>" value="<?php echo $instance['slug']; ?>" /></p>

        <p>
            <label for="<?php echo $this->get_field_name( 'image' ); ?>"><?php _e( 'Image:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'image' ); ?>" id="<?php echo $this->get_field_id( 'image' ); ?>" class="widefat" type="text" size="36"  value="<?php echo $instance['image']; ?>" />
            <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_name( 'logo' ); ?>"><?php _e( 'Logo:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'logo' ); ?>" id="<?php echo $this->get_field_id( 'logo' ); ?>" class="widefat" type="text" size="36"  value="<?php echo $instance['logo']; ?>" />
            <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
        </p>
		<?php
	}
}
