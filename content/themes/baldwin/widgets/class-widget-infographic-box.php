<?php
/**
 * Widget API: Baldwin_Widget_Quote_Box class
 */

/**
 * Core class used to implement a Text widget.
 *
 * @see WP_Widget
 */
class Baldwin_Widget_Infographic_Box extends WP_Widget {

	/**
	 * Sets up a new Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'baldwin_widget_infographic_box', 'description' => __('Box containing an Infographic, with a mobile fallback, styled for the Baldwin site.'));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('baldwin_infographic_box', __('Baldwin Infographic Box'), $widget_ops, $control_ops);

        add_action( 'admin_enqueue_scripts', array( $this, 'upload_scripts' ) );
	}

    /**
     * Upload the Javascripts for the media uploader
     */
    public function upload_scripts() {
        wp_enqueue_script( 'media-upload' );
        wp_enqueue_script( 'thickbox' );
        wp_enqueue_script( 'upload_media_widget', get_template_directory_uri() . '/widgets/upload-media.js', array( 'jquery' ) );

        wp_enqueue_style( 'thickbox' );
    }

	/**
	 * Outputs the content for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Text widget instance.
	 */
	public function widget( $args, $instance ) {
        $widget_image = ! empty( $instance['image'] ) ? $instance['image'] : '';
        $widget_list = ! empty( $instance['list'] ) ? $instance['list'] : '';
        $items = explode(PHP_EOL, $widget_list);

		echo $args['before_widget'];
        ?>

            <div class="l-v-margin xxlarge l-ignore-overlay process-infographic" style="background-image: url('<?php echo $widget_image; ?>');"></div>
            <div class="row l-ignore-overlay mobile-process-infographic">
                <div class="fourteen columns centered">

                <?php
                    foreach ( $items as $item ) :
                        preg_match( '/\[([0-9])\]\[(.*)\](.*)/', $item, $stuff );
                ?>

                    <?php if ( empty( $stuff ) ) : ?>
                        <div><h6 class="italic"><span></span><?php echo $item; ?></h6></div>
                    <?php else : ?>
                        <div><h6><span style="background-color: <?php echo $stuff[2]; ?>;"><?php echo $stuff[1]; ?></span><?php echo $stuff[3]; ?></h6></div>
                    <?php endif; ?>

                <?php
                    endforeach;
                ?>

                </div>
            </div>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
        $instance['image'] = $new_instance['image'];
        $instance['list']  = $new_instance['list'];
		return $instance;
	}

	/**
	 * Outputs the Text widget settings form.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'image' => '', 'list' => '' ) );
		?>

        <p>
            <label for="<?php echo $this->get_field_name( 'image' ); ?>"><?php _e( 'Image:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'image' ); ?>" id="<?php echo $this->get_field_id( 'image' ); ?>" class="widefat" type="text" size="36"  value="<?php echo $instance['image']; ?>" />
            <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
        </p>

        <h3>Mobile Text</h3>
        <p>Format items, one item per line.<br/>
        A line beginning with "[x][color] Blah Blah" will be formatted with a numbered icon with the given background color. The background color should be the full hex color, beginning with "#".<br/>
        A line without this info will be formatted with italics and an empty space where the icon would be.</p>

        <p><textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('list'); ?>" name="<?php echo $this->get_field_name('list'); ?>"><?php echo esc_textarea( $instance['list'] ); ?></textarea></p>

		<?php
	}
}
