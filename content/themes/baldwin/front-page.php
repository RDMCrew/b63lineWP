<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package rdmgumby
 */
get_header();
//baldwin_output_floating_nav( null, true );

$args = array(
    'post_type' => 'home_sections',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'menu_order',
    'order' => 'ASC'
);

$posts = get_posts( $args );
$i     = 0;

if ( $posts ) :
    foreach ( $posts as $post ) :
        setup_postdata( $post );

        $color_field = get_field_object( 'color_theme' );
        $color = get_field( 'color_theme' );
        $color_label = strtolower( $color_field['choices'][$color] );

        $stroke_color = $color;
        if ( get_field( 'track_position', $post->ID ) === 'start' )
            $stroke_color = '#fff';
?>

<div class="l-full-section dark typography waypoint" data-target="section-<?php echo $i; ?>" data-bubble="<?php echo $i; ?>">
    <?php if ( get_field( 'has_button', $post->ID ) ) : ?>
        <a class="home-section-link" href="<?php __the_field( 'button_link', 'esc_url', $post->ID ); ?>"></a>
    <?php endif; ?>

<?php if ( get_field( 'background_type', $post->ID ) === 'video' ) : ?>
    <video class="home-video" autoplay loop poster="<?php __the_field( 'poster', 'esc_url', $post->ID ); ?>">
        <source src="<?php __the_field( 'background_video', 'esc_url', $post->ID ); ?>" type="video/mp4" />
    </video>
    <div id="video-fallback" class="background" style="background-image: url('<?php __the_field( 'poster', 'esc_url', $post->ID ); ?>');"></div>
    <div id="video-overlay" class="gray overlay"></div>
<?php elseif ( get_field( 'background_type', $post->ID ) === 'image' ) : ?>
    <?php rdmgumby_enqueue_responsive_background( '#the-bg-'.$i, get_field( 'background_image', $post->ID ) ); ?>
    <div id="the-bg-<?php echo $i; ?>" class="background fixable">
        <div class="gray overlay"></div>
        <div class="gradient overlay" style="background: linear-gradient(rgba(0, 0, 0, 0), <?php echo $color; ?>); opacity: 0.4;"></div>
    </div>
<?php endif; ?>

    <div class="l-ignore-overlay">
        <svg class="track-piece" data-position="<?php __the_field( 'track_position', 'esc_attr', $post->ID ); ?>" data-icon="<?php __the_field( 'track_icon', 'esc_attr', $post->ID ); ?>" style="stroke: <?php echo $stroke_color; ?>;"></svg>

        <div class="row copy">

        <?php if ( get_field( 'is_contact_section', $post->ID ) ) : ?>
            <div class="twelve columns push_two">
                <h1 class="no-pad">
                <?php if ( get_field( 'clickable_title', $post->ID ) ) : ?>
                    <a href="<?php __the_field( 'title_link', 'esc_url', $post->ID ); ?>">
                        <?php echo get_the_title( $post->ID ); ?>
                    </a>
                <?php else : ?>
                    <?php echo get_the_title( $post->ID ); ?>
                <?php endif; ?>
                </h1>
                <hr class="border-brown" />
                <div class="row">
                    <?php baldwin_output_business_info( 'five columns business-info internal' ); ?>
                    <div class="six columns business-info external">
                        <?php
                            baldwin_output_business_logos();
                            baldwin_output_social_links();
                        ?>
                        <p class="l-v-margin tiny copyright"><?php __the_field( 'copyright_notice', 'esc_html', 'options' ); ?></p>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <div class="nine columns push_two">
                <h1 class="no-pad"><?php echo get_the_title( $post->ID ); ?></h1>
                <hr class="border-<?php echo $color_label; ?>" />
                <?php the_field( 'copy', $post->ID ); ?>
            <?php if ( get_field( 'sub_copy', $post->ID ) !== '' ) : ?>
                <h4 class="light text-<?php echo $color_label; ?>"><?php __the_field( 'sub_copy', 'esc_html', $post->ID ); ?></h4>
            <?php endif; ?>
            </div>
        <?php endif; ?>

        </div>
    </div>

<?php if ( get_field( 'has_button', $post->ID ) ) : ?>
    <div class="row l-v-margin xlarge call-to-action">
        <div class="ten columns push_two">
            <div class="button solid mobile-text-center <?php echo $color_label; ?>">
                <a href="<?php __the_field( 'button_link', 'esc_url', $post->ID ); ?>"><?php __the_field( 'button_text', 'esc_html', $post->ID); ?></a>
            </div>
        </div>
    </div>
<?php endif; ?>

</div>

<?php
        $i++;
    endforeach;
endif;
wp_reset_postdata();

rdmgumby_output_responsive_backgrounds();
add_action( 'wp_footer', 'baldwin_build_tracks', 99 );
get_footer();

function baldwin_build_tracks() {
    echo '<script>fixedBackgrounds.init();</script>';
    echo '<script>trackBuilder.init();</script>';
    echo '<script>floatingNav.init();</script>';
}
