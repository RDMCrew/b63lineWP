<?php
/**
 * Standard Post Format template
 */
$page_id = get_option( 'page_for_posts' );
?>

<h1 class="no-pad"><?php the_title(); ?></h1>
<hr style="border-color: <?php __the_field( 'color_theme', 'esc_attr', $page_id ); ?>;" />
<?php the_content(); ?>
