<?php
/**
 * functions.php
 *
 * All functionality specific to the custom theme lives here.
 * Note that if you're building a theme based on rdmgumby, you'll want to find and replace
 * the rdmgumby package name with your own package name.
 *
 * @package rdmgumby
 */

/**
 * Library functions that act independently of the theme templates.
 */
require_once get_template_directory() . '/inc/lib.php';

/**
 * Sets up the theme and registers support for WordPress features
 *
 * Note this is hooked into after_setup_theme, which runs before the init hook.
 * The init hook is too late for some features
 */
add_action( 'after_setup_theme', 'rdm_gumby_setup' );
function rdm_gumby_setup()
{
	// Make the theme available for translations. Complete and install translations into ./languages/
	load_theme_textdomain( 'rdmgumby', get_template_directory() . '/languages' );

	// Enable support for Post Thumbnails on posts and pages.
	add_theme_support( 'post-thumbnails' );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'video', 'link', 'gallery' ) );

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form', 'gallery', 'caption' ) );

	// Enable default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Register Nav Menus
	if ( function_exists( 'rdmgumby_register_nav_menus' ) ) {
		rdmgumby_register_nav_menus();
	}

	// Register sidebars
	if ( function_exists( 'rdmgumby_widgets_init' ) ) {
		rdmgumby_widgets_init();
	}

	// Set up the custom post types, if there are any
	// we build out our CPT using a custom plugin with the below function
	if ( function_exists( 'theme_custom_post_types' ) ) {
		theme_custom_post_types();
	}
}

/**
 * Registers Nav Menus for the theme. Add array entries as needed.
 * If not registering any menus, you can safely delete this function
 *
 * For new menus, follow the format
 * 'menu-slug' => __( 'menu-name', 'textdomain' ),
 */
function rdmgumby_register_nav_menus()
{
	register_nav_menus( array(
            'bubble' => __( 'Bubble Nav', 'rdmgumy' ),
			'mobile' => __( 'Mobile Menu', 'rdmgumby' )
		) );
}

/**
 * Register any Sidebars for the theme
 * If not registering any sidebars, you can safely delete this function
 *
 * For new sidebars, copy and paste the register sidebar function
 */
function rdmgumby_widgets_init()
{
	register_sidebar( array(
			'name'          => __( 'Sidebar', 'rdmgumby' ),
			'id'            => 'sidebar-1',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>'
		));

    register_sidebar( array(
            'name'          => __( 'Instagram Feed', 'rdmgumby' ),
            'id'            => 'sidebar-instagram',
            'description'   => '',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h1 class="widget-title">',
            'after_title'   => '</h1>'
        ));

    // import custom widgets
    require_once get_template_directory() . '/widgets/class-widget-quote-box.php';
    require_once get_template_directory() . '/widgets/class-widget-text-box.php';
    require_once get_template_directory() . '/widgets/class-widget-service-box.php';
    require_once get_template_directory() . '/widgets/class-widget-title-text-box.php';
    require_once get_template_directory() . '/widgets/class-widget-call-to-action-box.php';
    require_once get_template_directory() . '/widgets/class-widget-title-text-image-box.php';
    require_once get_template_directory() . '/widgets/class-widget-collaborators-box.php';
    require_once get_template_directory() . '/widgets/class-widget-staff-box.php';
    require_once get_template_directory() . '/widgets/class-widget-twitter-text-box.php';
    require_once get_template_directory() . '/widgets/class-widget-case-study-box.php';
    require_once get_template_directory() . '/widgets/class-widget-infographic-box.php';
    require_once get_template_directory() . '/widgets/class-widget-video-embed-box.php';

    // register custom widgets
    add_action ('widgets_init', function() {
        register_widget( 'Baldwin_Widget_Quote_Box' );
        register_widget( 'Baldwin_Widget_Text_Box' );
        register_widget( 'Baldwin_Widget_Service_Box' );
        register_widget( 'Baldwin_Widget_Title_Text_Box' );
        register_widget( 'Baldwin_Widget_Call_To_Action_Box' );
        register_widget( 'Baldwin_Widget_Title_Text_Image_Box' );
        register_widget( 'Baldwin_Widget_Collaborators_Box' );
        register_widget( 'Baldwin_Widget_Staff_Box' );
        register_widget( 'Baldwin_Widget_Twitter_Text_Box' );
        register_widget( 'Baldwin_Widget_Case_Study_Box' );
        register_widget( 'Baldwin_Widget_Infographic_Box' );
        register_widget( 'Baldwin_Widget_Video_Embed_Box' );
    });
}

/**
 * Include Advanced Custom Fields as a required plugin
 */

	/**
	 * This includes the free version (4.3.9) of ACF and is included with this theme
	 * Comment this out if you are using ACF Pro
	 */
	//include_once( get_template_directory() . '/inc/advanced-custom-fields/acf.php' );

	/**
	 * This includes ACF Pro, but you must install into ./inc/ yourself
	 * If using ACF Pro, simply uncomment all of the following code
	 */
	add_filter( 'acf/settings/path', 'acfSettingsPath' );
	function acfSettingsPath( $path )
	{
	    $path = get_template_directory() . '/inc/advanced-custom-fields-pro/';
	    return $path;
	}

	add_filter( 'acf/settings/dir', 'acfSettingsDir' );
	function acfSettingsDir( $dir )
	{
	    $dir = get_template_directory_uri() . '/inc/advanced-custom-fields-pro/';
	    return $dir;
	}

	include_once( get_template_directory() . '/inc/advanced-custom-fields-pro/acf.php' );

/**
 * Enqueue scripts and styles
 *
 * Note that we enqueue minified versions of all of these files. If you are not
 * using minified files, you may want to modify the enqueues here. Or more likely,
 * you'll want to start using minified files.
 *
 * Note that you can uncomment the vendor-style and vendor-script enqueues if you
 * need them for your theme. They are used if you have bower components installed
 * that will create css or js files via gulp.
 */
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts', 99 );
function theme_enqueue_scripts() {
    wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/style.min.css' );
    wp_enqueue_style( 'vendor-style', get_template_directory_uri() . '/vendor.min.css' );

    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'modernizr-script', get_template_directory_uri() . '/assets/js/dist/modernizr-3.3.1.min.js', array(), '3.3.1', false );
    wp_enqueue_script( 'gumby-script', get_template_directory_uri() . '/assets/js/dist/gumby.min.js', array(), '2.6.4', true );
    wp_enqueue_script( 'vendor-script', get_template_directory_uri() . '/assets/js/dist/vendor.min.js', array(), '', true );
    wp_enqueue_script( 'theme-script', get_template_directory_uri() . '/assets/js/dist/all.min.js', array(), '', true );
}

/**
 * Features you can enable or disable as needed.
 */

	// Implement the Custom Header feature.
	//require get_template_directory() . '/inc/custom-header.php';

	// Customizer additions.
	//require get_template_directory() . '/inc/customizer.php';

	// Load Jetpack compatibility file.
	//require get_template_directory() . '/inc/jetpack.php';

	// Add support for automatic creation of alt tags for images in the content
	add_filter( 'the_content', 'rdmgumby_add_alt_tags', 9999 );

	// Add support for including <p> tags in the excerpts
	remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );
	add_filter( 'get_the_excerpt', 'rdmgumby_trim_excerpt' );

    // add support for responsive background images from the media library
    //add_action( 'wp_footer', 'rdmgumby_output_responsive_backgrounds', 99 );

    // Sets up the theme color
    // this is used as the ms tile background color and the chrome toolbar color
    global $favicon_theme_color;
    $favicon_theme_color = '#a86401';
    // Adds generated favicons to theme from end and backend
    // http://realfavicongenerator.net/
    add_action( 'wp_head', 'rdmgumby_output_favicons' );
    add_action( 'admin_head', 'rdmgumby_output_favicons' );
    add_action( 'login_head', 'rdmgumby_output_favicons' );

// turns off wordpress emojis for the front end because there's a bug in
// wp 4.2.2 that causes JS errors when using SVG
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 *
 */
function baldwin_output_floating_nav( $current = null, $skiplinks = false )
{
    $menu_name  = 'bubble';
    $locations  = get_nav_menu_locations();
    $menu       = wp_get_nav_menu_object( $locations[$menu_name] );
    $menu_items = wp_get_nav_menu_items( $menu->term_id );
?>
    <div id="floating-nav" class="floating-nav">
        <ul>

<?php
    $i = 1;
    foreach( $menu_items as $key => $item ) :
        $title = strtolower( $item->title );
        $url   = $item->url;
?>
            <li id="bubble-<?php echo $i; ?>" class="toggle current" gumby-classname="is-text" gumby-on="mouseover mouseout">
                <img id="<?php echo $title; ?>-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-<?php echo $title; ?>.svg" class="icon-color js-blur" />
                <img id="<?php echo $title; ?>-icon-black" src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-<?php echo $title; ?>-black.svg" class="icon-black" />
                <p id="<?php echo $title; ?>-text" class="small ubuntu light uppercase text-white no-margin">
                <?php if ( $skiplinks ) : ?>
                    <a href="#" class="skip" gumby-goto="[data-target='section-<?php echo $i; ?>']"><?php echo $title; ?></a>
                <?php else : ?>
                    <a href="<?php echo $url; ?>"><?php echo $title; ?></a>
                <?php endif; ?>
                </p>
            </li>
<?php
        $i++;
    endforeach;
?>

        </ul>
    </div>
<?php
}

/**
 *
 */
function baldwin_output_team_bios()
{
    $posts = array();

    $post_args = array(
        'post_type' => 'staff',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'menu_order',
        'order' => 'ASC'
    );

    $posts = get_posts( $post_args );

    if ( $posts ) :
        foreach ( $posts as $post ) :
            setup_postdata( $post );

            $bio_id = strtolower( str_replace( ' ', '', get_the_title( $post->ID ) ) );
?>

    <div id="<?php echo $bio_id; ?>" class="team-bio bg-bg-gray dark typography">
        <h3 class="close-button"><a href="#" class="text-white"><i class="icon-cancel"></i></a></h3>
        <div class="team-bio-pic">
            <img src="<?php __the_field( 'picture', 'esc_url', $post->ID ); ?>" />
        </div>
        <div class="team-bio-copy small bg-bg-gray">
            <h2 class="no-pad"><?php echo get_the_title( $post->ID ); ?></h2>
            <hr style="border-color: <?php __the_field( 'color_theme', 'esc_attr', $post->ID ); ?>;" />
            <h4 class="smaller merriweather italic"><?php __the_field( 'position', 'esc_html', $post->ID ); ?></h4>
            <?php echo get_field( 'bio', $post->ID ); ?>
        </div>
    </div>

<?php
        endforeach;
    endif;
    wp_reset_postdata();
}

/**
 *
 */
function baldwin_output_business_logos()
{
?>
    <div class="business-logos">
        <span><a href="http://www.bbb.org/" target="_blank"><?php include( get_template_directory() . '/assets/img/icon-bbb-logo.svg' ); ?></a></span>
        <span><a href="http://www.bbb.org/" target="_blank"><?php include( get_template_directory() . '/assets/img/icon-bbb-award.svg' ); ?></a></span>
        <span><a href="http://www.bizjournals.com/dayton/" target="_blank"><?php include( get_template_directory() . '/assets/img/icon-roty-logo.svg' ); ?></a></span>
        <span><a href="http://gdusa.com/" target="_blank"><?php include( get_template_directory() . '/assets/img/icon-gdusa-logo.svg' ); ?></a></span>
    </div>
<?php
}

/**
 *
 */
function baldwin_output_social_links()
{
?>
    <h5 class="l-v-margin light social-links">

<?php
    if ( have_rows( 'social_media_links', 'options' ) ) {
        while ( have_rows( 'social_media_links', 'options' ) ) {
            the_row();
?>
        <span><a href="<?php __the_sub_field( 'url', 'esc_url' ); ?>" target="_blank"><?php __the_sub_field( 'type' ); ?></a></span>
<?php
        }
    }
?>
    </h5>
<?php
}

/**
 *
 */
function baldwin_output_business_info( $wrapper = '' )
{
?>
    <div class="<?php echo $wrapper; ?>">
        <h5 class="regular">Dayton Office</h5>
        <p class="l-v-margin small">
            <a href="<?php __the_field( 'dayton_office_map', 'esc_url', 'options' ); ?>" target="_blank"><?php the_field( 'dayton_office_address', 'options' ); ?></a><br />
            <a href="tel:+1<?php echo str_replace( '.', '', get_field( 'dayton_office_phone', 'options' ) ); ?>"><?php __the_field( 'dayton_office_phone', 'esc_html', 'options' ); ?></a><br />
            <a href="mailto:<?php echo antispambot( get_field( 'dayton_office_email', 'options' ) ); ?>"><?php echo antispambot( get_field( 'dayton_office_email', 'options' ) ); ?></a>
        </p>
    </div>
    <div class="<?php echo $wrapper; ?>">
        <h5 class="regular">Louisville Office</h5>
        <p class="l-v-margin small">
            <a href="<?php __the_field( 'louisville_office_map', 'esc_url', 'options' ); ?>" target="_blank"><?php the_field( 'louisville_office_address', 'options' ); ?></a><br />
            <a href="tel:+1<?php echo str_replace( '.', '', get_field( 'louisville_office_phone', 'options' ) ); ?>"><?php __the_field( 'louisville_office_phone', 'esc_html', 'options' ); ?></a><br />
            <a href="mailto:<?php echo antispambot( get_field( 'louisville_office_email', 'options' ) ); ?>"><?php echo antispambot( get_field( 'louisville_office_email', 'options' ) ); ?></a>
        </p>
    </div>
<?php
}

/**
 * This filters the select fields specified in the filters, and populates it
 * with the values from the color options
 *
 * @param array $field - the acf field object
 * @return array - the modified field object
 */
add_filter( 'acf/load_field/name=color_theme', 'rdmgumby_populate_color_field' );
function rdmgumby_populate_color_field( $field )
{
    $field['choices'] = array();

    if ( have_rows( 'colors', 'options' ) ) {
        while ( have_rows( 'colors', 'options' ) ) {
            the_row();
            $value = get_sub_field( 'color' );
            $label = get_sub_field( 'label' );

            $field['choices'][$value] = $label;
        }
    }

    return $field;
}

/**
 * Convert a Hex value to an rgb string, optionally an rgba string.
 *
 * @param string $hex - the hex value, can include # or not
 * @param number $alpha - optionally pass an alpha value
 * @return string - the converted rgb/a string
 */
function rdmgumy_hex2rgb( $hex, $alpha ) {
    $hex = str_replace( "#", "", $hex );

    if( strlen( $hex ) === 3 ) {
        $r = hexdec( substr( $hex, 0, 1 ).substr( $hex, 0, 1 ) );
        $g = hexdec( substr( $hex, 1, 1 ).substr( $hex, 1, 1 ) );
        $b = hexdec( substr( $hex, 2, 1 ).substr( $hex, 2, 1 ) );

    } else {
        $r = hexdec( substr( $hex, 0, 2 ) );
        $g = hexdec( substr( $hex, 2, 2 ) );
        $b = hexdec( substr( $hex, 4, 2 ) );

    }

    $rgb = ( ! empty( $alpha ) ) ? array( $r, $g, $b, $alpha ) : array( $r, $g, $b );

    return implode(",", $rgb);
}

/**
 * Reorders the roles array to put Web Admin right before Administrator.
 * Removes the Administrator role if the user is not an admin.
 *
 * @param array $all_roles The WordPress roles array
 * @return array The updated roles array.
 */
add_filter( 'editable_roles', 'rdmgumby_filter_editable_roles' );
function rdmgumby_filter_editable_roles( $all_roles )
{
    $roles = array( 'administrator' => $all_roles['administrator'],
                    'web_admin'     => $all_roles['web_admin']
                );
    foreach ( $all_roles as $role => $details ) {
        if ( $role !== 'administrator' && $role !== 'web_admin' ) {
            $roles[$role] = $all_roles[$role];
        }
    }
    // removes the administrator role from the list if the user is not an adminstrator
    if ( !current_user_can( 'manage_administrators' ) )
        unset( $roles['administrator'] );
    return $roles;
}

/**
 * Hide ACF in the admin area for non administrators
 */
add_filter( 'acf/settings/show_admin', 'rdmgumby_acf_show_admin' );
function rdmgumby_acf_show_admin( $show ) {
    if ( current_user_can( 'manage_acf' ) )
        return true;
    else
        return false;
}
