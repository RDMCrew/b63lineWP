<?php
/**
 * @package rdmgumby
 */

$page_id = get_option( 'page_for_posts' );

if ( has_post_thumbnail( $page_id ) )
    rdmgumby_enqueue_responsive_background( '#the-bg', get_post_thumbnail_id( $page_id ) );

$hero_class = 'l-full-section';
if ( get_field( 'hero_style', $page_id ) === 'short' )
    $hero_class = 'l-section';

get_header(); ?>

<div class="page">
    <div class="hero dark typography <?php echo $hero_class; ?>">
        <div id="the-bg" class="background"><div class="gray overlay"></div></div>
        <div class="l-ignore-overlay">

            <div class="row copy">
                <div class="eleven columns centered text-center">
                    <h6 class="light spaced uppercase"><?php echo get_the_title( $page_id ); ?></h6>
                    <h1 class="no-pad"><?php __the_field( 'main_copy', 'esc_html', $page_id ); ?></h1>
                    <hr class="center" style="border-color: <?php __the_field( 'color_theme', 'esc_attr', $page_id ); ?>;" />
                    <?php the_field( 'sub_copy', $page_id ); ?>
                </div>
            </div>
        </div>
    </div>

    <?php
        if ( have_posts() ) :
            $i = 1;
            while ( have_posts() ) :
                the_post();
                $cats = get_the_category();
    ?>

    <a href="<?php echo esc_url( get_permalink() ); ?>" class="blog-item toggle" gumby-classname="is-active" gumby-trigger="#blog-item-<?php echo $i; ?>" gumby-on="mouseover mouseout">
        <div id="blog-item-<?php echo $i; ?>" class="l-section l-padded-small light typography">
            <div class="row">
                <div class="fourteen columns centered text-center">
                    <h6 class="light spaced uppercase"><?php echo $cats[0]->name; ?></h6>
                    <h2 class="no-pad"><?php the_title(); ?></h2>
                    <hr class="center" />
                    <p class="text-center"><?php echo get_the_excerpt(); ?></p>
                </div>
            </div>
        </div>
    </a>

    <?php
                $i++;
            endwhile;
        endif;
    ?>

</div>

<?php
rdmgumby_output_responsive_backgrounds();
get_footer();
