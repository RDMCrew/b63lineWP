<?php
/**
 * Video Post Format template
 */
$page_id = get_option( 'page_for_posts' );
?>

<h1 class="no-pad"><?php the_title(); ?></h1>
<hr style="border-color: <?php __the_field( 'color_theme', 'esc_attr', $page_id ); ?>;" />
<?php the_content(); ?>
<div id="gallery-slider" class="owl-carousel">

<?php
    $images = get_field( 'gallery' );

    if ( $images ) :
        foreach ( $images as $image ) :
?>
            <div class="text-center">
                <a href="<?php echo $image['url']; ?>">
                    <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
            </div>
<?php
        endforeach;
    endif;
?>

</div>

<?php
add_action( 'wp_footer', 'baldwin_init_gallery_slider', 99 );
function baldwin_init_gallery_slider() {
    echo '<script>carousels.initGallerySlider();</script>';
}
