var twitterCarousel = ( function ($) {

    function init() {
        $('#twitter-carousel').owlCarousel({
            singleItem: true
        });
    }

    return {
        init: init
    };
}(jQuery));

var carousels = ( function($) {
    var homeSlider    = null,
        gallerySlider = null;

    function initHomeSlider() {
        $('#home-slider').owlCarousel({
            singleItem: true,
            autoPlay: true,
            stopOnHover: true,
            theme: ""
        });

        homeSlider = $('#home-slider').data('owlCarousel');
        $('#home-slider-prev').on('click', sliderNavPrevHandler);
        $('#home-slider-next').on('click', sliderNavNextHandler);
        $('.home-slider-page').on('click', sliderNavPageHandler);
    }

    function initGallerySlider() {
        $('#gallery-slider').owlCarousel({
            singleItem: true,
            autoPlay: true,
            stopOnHover: true
        });

        gallerySlider = $('#gallery-slider').data('owlCarousel');
    }

    var sliderNavPrevHandler = function (e) {
        e.preventDefault();
        homeSlider.prev();
    };

    var sliderNavNextHandler = function (e) {
        e.preventDefault();
        homeSlider.next();
    };

    var sliderNavPageHandler = function (e) {
        var $el     = $(this);
        var pageNum = $el.data('page-num');

        $('.home-slider-page').removeClass('active');
        $el.addClass('active');

        homeSlider.goTo(pageNum);
    };

    return {
        initHomeSlider: initHomeSlider,
        initGallerySlider: initGallerySlider
    };

}(jQuery));
