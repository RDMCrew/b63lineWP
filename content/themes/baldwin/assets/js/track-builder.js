var trackBuilder = ( function ($) {
    var points = [], curPos = { 'x': 0, 'y': 0 }, curPoint = { 'x': 0, 'y': 0 },
        curIndex, scrollTop = 0, pathLength, path, curLength = 0;

    var heroTrack = d3.select('#hero-track'),
        aboutTrack = d3.select('#about-track'),
        servicesTrack = d3.select('#services-track'),
        workTrack = d3.select('#work-track'),
        blogTrack = d3.select('#blog-track'),
        contactTrack = d3.select('#contact-track');

    var $tracks     = $('.track-piece'),
        trackPieces = [];

    function init() {
        $tracks.each(function (index) {
            var track = {
                element: $(this),
                position: $(this).data('position'),
                icon: $(this).data('icon'),
                d3Obj: d3.select(this)
            };

            trackPieces.push(track);
        });

        build();
        $(window).resize(build);
    }

    var build = function() {
        d3.selectAll('.track-piece path').remove();
        d3.selectAll('.track-piece image').remove();
        d3.selectAll('.track-piece circle').remove();

        var w = $(window).width(),
            h = $(window).height(),
            imgOffset = 15,
            wTwenty   = Math.round(w * 0.15),
            //wFifteen  = Math.round(w * 0.1),
            wFifteen  = wTwenty - 50,
            wOneThird = Math.round(w / 3),
            hOneThird = Math.round(h * 0.25),
            wTwoThird = wOneThird * 2 + 50,
            hTwoThird = hOneThird * 2,
            wOneThirdMinus = wOneThird - 50,
            wOneThirdPlus  = wOneThird + 50,
            hOneThirdMinus = hOneThird - 50,
            hOneThirdPlus  = hOneThird + 50,
            wTwoThirdMinus = wTwoThird - 50,
            wTwoThirdPlus  = wTwoThird + 50,
            hTwoThirdMinus = hTwoThird - 50,
            hTwoThirdPlus  = hTwoThird + 50,
            imageX = wTwenty - imgOffset,
            imageY = hOneThird - imgOffset;

        var startP1 = wTwenty + ' ' + hOneThird,
            startP2 = wTwoThird + ' ' + hOneThird,
            startP3 = wTwoThirdPlus + ' ' + hOneThirdPlus,
            startP4 = wTwoThirdPlus + ' ' + h,
            endP1   = wTwoThirdPlus + ' 0',
            endP2   = wTwoThirdPlus + ' ' + hOneThirdMinus,
            endP3   = wTwoThird + ' ' + hOneThird,
            endP4   = wTwenty + ' ' + hOneThird,
            leftP1  = wTwoThirdPlus + ' 0',
            leftP2  = wTwoThirdPlus + ' ' + hOneThirdMinus,
            leftP3  = wTwoThird + ' ' + hOneThird,
            leftP4  = wTwenty + ' ' + hOneThird,
            leftP5  = wFifteen + ' ' + hOneThirdPlus,
            leftP6  = wFifteen + ' ' + h,
            rightP1 = wFifteen + ' 0',
            rightP2 = wFifteen + ' ' + hOneThirdMinus,
            rightP3 = wTwenty + ' ' + hOneThird,
            rightP4 = wTwoThird + ' ' + hOneThird,
            rightP5 = wTwoThirdPlus + ' ' + hOneThirdPlus,
            rightP6 = wTwoThirdPlus + ' ' + h;

        var startPath = [startP1, startP2, startP3, startP4],
            endPath   = [endP1, endP2, endP3, endP4],
            leftPath  = [leftP1, leftP2, leftP3, leftP4, leftP5, leftP6],
            rightPath = [rightP1, rightP2, rightP3, rightP4, rightP5, rightP6];

        $.each(trackPieces, function (index, track) {
            if (track.position === 'start') {
                track.d3Obj.append('path').attr('d', buildPath(startPath));
                track.d3Obj.append('circle').attr('cx', wTwenty).attr('cy', hOneThird).attr('r', '20');

            } else if (track.position === 'end') {
                track.d3Obj.append('path').attr('d', buildPath(endPath));
                track.d3Obj.append('circle').attr('cx', wTwenty).attr('cy', hOneThird).attr('r', '20');

            } else if (track.position === 'left') {
                track.d3Obj.append('path').attr('d', buildPath(leftPath));

            } else if (track.position === 'right') {
                track.d3Obj.append('path').attr('d', buildPath(rightPath));

            }

            track.d3Obj
                .append('image')
                .attr('xlink:href', 'content/themes/baldwin/assets/img/icon-'+track.icon+'.svg')
                .attr('x', imageX)
                .attr('y', imageY)
                .attr('width', '30')
                .attr('height', '30');
        });
    };

    var buildPath = function (points) {
        var s = '';

        $.each(points, function (index, value) {
            var pre = (index === 0) ? 'M' : ' L';
            s += pre + value;
        });

        return s;
    };

    return {
        init: init
    };

})(jQuery);
