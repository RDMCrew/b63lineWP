var motionBlur = ( function ($) {
    var filters    = document.querySelector(".filters"), // the SVG that contains the filters
        defs       = filters.querySelector("defs"), // the  element inside the SVG
        blur       = defs.querySelector("#blur"), // the blur filter
        blurFilter = blur.firstElementChild; // the feGaussianBlur primitive

    // the element we want to apply the effect
    var $element=$("#about-icon");
    // storing the last position, to be able to measure changes
    //var lastPos=$element.offset();
    // a multiplier, to be able to control the intensity of the effect
    var multiplier=0.25;

    function init() {
        // go through all the objects that need a blur filter
        $(".js-blur").each(function(i){
            // clone the filter
            var blurClone=blur.cloneNode(true);

            // create and set a new ID so we can use the filter through CSS
            var blurId="blur"+i;
            blurClone.setAttribute("id",blurId);

            defs.appendChild(blurClone);

            // set the CSS
            var filter="url(#"+blurId+")";
            $(this)
                .css({
                    webkitFilter:filter,
                    filter:filter
                })
                // store the filter reference on the object for practicity
                .data("blur",blurClone)
                .data("lastPos", $(this).offset());
        });
        updateMotionBlur();
    }

    var setBlur = function (el, x, y) {
        var blur = el.data("blur");
        blur.firstElementChild.setAttribute("stdDeviation",x+","+y);
        //blur.firstElementChild.setAttribute("stdDeviation","2,2");
    };

    var updateMotionBlur = function () {
        $(".js-blur").each(function(i){
            // get the current position of the element
            var currentPos=$(this).offset();
            var lastPos=$(this).data("lastPos");

            // calculate the changes from the last frame and apply the multiplier
            var xDiff=Math.abs(currentPos.left-lastPos.left)*multiplier;
            var yDiff=Math.abs(currentPos.top-lastPos.top)*multiplier;

            // set the blur
            setBlur($(this),xDiff,yDiff);

            // store current position for the next frame
            lastPos=currentPos;
            $(this).data("lastPos", lastPos);
        });

        // call to update in the next frame
        requestAnimationFrame(updateMotionBlur);
    };

    return {
        init: init
    };

})(jQuery);
