var fixedBackgrounds = ( function ($) {
    var $bgs = $('.background.fixable');

    function init() {
        $bgs.each(function (index, $bg) {
            new Waypoint.Sticky({
                element: $bg,
                wrapper: false
            });
        });
    }

    return {
        init: init
    };

})(jQuery);
