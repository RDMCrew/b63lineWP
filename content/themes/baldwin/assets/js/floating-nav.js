var floatingNav = ( function ($) {
    var $nav        = $('#floating-nav'),
        $bubbles    = $('li', $nav),
        $blackIcons = $('.icon-black', $bubbles),
        $currentBubble;

    function init() {
        var waypoints = $('.waypoint').waypoint(updateCurrentNavBubble, { offset: '50%' });
    }

    var updateCurrentNavBubble = function (direction) {
        var $el    = $(this.element),
            target = parseInt($el.data('bubble'));

        if (direction === 'up')
            target--;

        $blackIcons.removeClass('is-active');

        if (target === 0) {
            $bubbles.addClass('current');

        } else {
            $currentBubble = $('#bubble-'+target);

            $bubbles.removeClass('current');
            $currentBubble.addClass('current');
            $('li:not(.current) .icon-black', $nav).addClass('is-active');
        }
    };

    return {
        init: init
    };
})(jQuery);
