var teamBios = ( function ($) {
    var $page = $('#page'),
        $body = $('body'),
        $active;

    function init() {
        $('.team-bio-trigger').on('click', openHandler);
        $('.close-button').on('click', closeHandler);
    }

    function openHandler (e) {
        e.preventDefault();
        var bio = $(this).data('bio'),
            $el = $(bio);

        $body.addClass('is-pushed');
        $page.addClass('is-pushed');
        $el.addClass('is-pushed');

        if ($active && !$active.is($el))
            $active.removeClass('is-pushed');
        $active = $el;

        setTimeout(function () {
            $(document).on('keydown', closeHandler);
        }, 100);
    }

    function closeHandler (e) {
        if (!e.keyCode || e.keyCode === 27) {
            e.preventDefault();
            $body.removeClass('is-pushed');
            $page.removeClass('is-pushed');
            $('.team-bio').removeClass('is-pushed');
        }

        $(document).off('keydown', closeHandler);
    }

    return {
        init: init
    };

})(jQuery);
