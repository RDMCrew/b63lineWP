var mobileMenu = ( function ($) {
    var $site, $button, menuActive;

    function init() {
        $site      = $('#page');
        $button    = $('#mobile-menu-button');
        menuActive = false;

        $button.on('touchend click', openMenu);
        $site.on('touchend click', closeMenu);
    }

    var openMenu = function (e) {
        e.preventDefault();
        if (!menuActive) {
            if (e.type === 'touchend')
                $(this).off('click');

            $site.addClass('is-nav-active');
            // need to wait a tick before we flag the menu as active, otherwise
            // it will just immediately remove the 'is-nav-active class
            setTimeout(function () {
                menuActive = true;
            }, 500);

            // let the user close the menu with the 'esc' key
            document.addEventListener('keydown', closeMenu);
        }
    };

    var closeMenu = function (e) {
        // 27 is the escape key
        if (menuActive && (!e.keyCode || e.keyCode === 27)) {
            if (e.type === 'touchend')
                $(this).off('click');

            $site.removeClass('is-nav-active');
            menuActive = false;

            // menu is closed, don't care about 'esc' key anymore
            document.removeEventListener('keydown', closeMenu);
        }
    };

    return {
        init: init
    };
})(jQuery);
