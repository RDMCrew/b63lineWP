var caseStudies = ( function ($) {
    function init() {
        var $oneCol  = $('.baldwin-one-column-row'),
            $twoCols = $('.baldwin-two-column-row');

        $oneCol.each(function (index) {
            var $el  = $(this),
                html = $el.html();

            $el.html('');
            $el.prepend('<div class="row">'+html+'</div>');
        });

        $twoCols.each(function (index) {
            var $el  = $(this),
                html = $el.html();

            $el.html('');
            $el.prepend('<div class="row">'+html+'</div>');
        });
    }

    return {
        init: init
    };

})(jQuery);
