var scrollArrows = ( function ($) {
    var $arrows = $('#scroll-arrows');

    function init() {
        setInterval(function () {
            $arrows.addClass('is-active');
            setTimeout(function () {
                $arrows.removeClass('is-active');
            }, 6000);
        }, 8000);
    }

    return {
        init: init
    };

})(jQuery);
