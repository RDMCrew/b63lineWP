<?php
/**
 * Template Name: Case Study Page
 * @package rdmgumby
 */
$sector = get_field( 'sector' );
$icon   = str_replace( '-', '', get_field( 'sector' ) );
get_header(); ?>

<div class="page case-study">
    <div class="l-section hero dark typography">
        <?php the_post_thumbnail(); ?>
    </div>

    <div class="l-section l-padded-tiny bg-black dark typography">
        <div class="row">
            <div class="fourteen columns centered">
                <h5 class="uppercase">
                    <span><img class="market-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-<?php echo $icon; ?>.svg" /></span>
                    <span class="market-copy"><?php echo $sector; ?></span>
                </h5>
            </div>
        </div>
    </div>

    <div class="nav-arrows clearfix">
        <?php previous_post_link( '%link', '<div class="nav-arrow left"><i class="icon-arrow-left"></i></div>' ); ?>
        <?php next_post_link( '%link', '<div class="nav-arrow right"><i class="icon-arrow-right"></i></div>' ); ?>
    </div>

    <div class="l-section l-padded-small bg-white light typography">
        <div class="row">
            <div class="fourteen columns centered">
                <div class="row">
                    <div class="eleven columns">
                        <h1 class="no-pad"><?php __the_field( 'tagline' ); ?></h1>
                    </div>
                </div>
                <div class="l-v-margin l-bordered">
                    <h5 class="light no-pad"><span class="regular">Client:</span> <?php __the_field( 'client' ); ?> <span class="l-spaced">/</span> <span class="regular">Services:</span> <?php __the_field( 'services' ); ?></h5>
                </div>
            </div>
        </div>
    </div>

    <?php
        if ( have_posts() ) :
            while ( have_posts() ) :
                the_post();
                the_content();
            endwhile;
        endif;
    ?>

</div>

<?php
add_action( 'wp_footer', 'baldwin_case_studies_init', 99 );
get_footer();

function baldwin_case_studies_init() {
    echo '<script>caseStudies.init();</script>';
}
