<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package rdmgumby
 */
$color_theme = ( ! empty( get_field( 'color_theme' ) ) ) ? get_field( 'color_theme' )
                                                         : get_field( 'color_theme', get_option( 'page_for_posts' ) );
?>
    <?php if ( !is_front_page() ) : ?>
        <div class="l-section l-padded-small dark typography" style="background-color: <?php echo $color_theme; ?>;">
            <div class="row">
                <div class="ten columns push_one l-v-margin">
                     <h4 class="no-margin mobile-text-center"><?php the_field( 'foot_note', 'options' ); ?></h4>
                </div>
                <div class="four columns l-v-margin">
                    <div class="button standard mobile-text-center">
                        <a href="<?php echo home_url(); ?>/contact">Let's Connect</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

	</div><!-- #content .site-content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

    <?php if ( !is_front_page() ) : ?>
        <div class="l-section l-padded-small bg-bg-gray dark typography clearfix">
            <div class="footer-section pushed text-center">
                <div class="footer-logo">
                    <a href="<?php echo home_url(); ?>"></a>
                    <?php the_field( 'full_logo_svg', 'options' ); ?>
                </div>
            </div>
            <?php baldwin_output_business_info( 'footer-section internal' ); ?>
            <div class="footer-section external">
                <?php
                    baldwin_output_business_logos();
                    baldwin_output_social_links();
                ?>
                <p class="l-v-margin tiny text-center"><?php __the_field( 'copyright_notice', 'esc_html', 'options' ); ?></p>
            </div>
        </div>
    <?php endif; ?>

	</footer><!-- #colophon .site-footer -->

</div><!-- #page -->

<?php wp_footer(); ?>
<!-- Hotjar Tracking Code for baldwincreative.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:90937,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
</body>
</html>
