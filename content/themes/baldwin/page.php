<?php
/**
 * @package rdmgumby
 */

if ( has_post_thumbnail() )
    rdmgumby_enqueue_responsive_background( '#the-bg', get_post_thumbnail_id() );

$hero_class = 'l-full-section';
if ( get_field( 'hero_style' ) === 'short' )
    $hero_class = 'l-section';

get_header(); ?>

<div class="page">
    <div class="hero dark typography <?php echo $hero_class; ?>">
        <div id="the-bg" class="background"><div class="gray overlay"></div></div>
        <div class="l-ignore-overlay">

            <div class="row copy">
                <div class="eleven columns centered text-center">
                    <h6 class="light spaced uppercase"><?php the_title(); ?></h6>
                    <h1 class="no-pad"><?php __the_field( 'main_copy' ); ?></h1>
                    <hr class="center" style="border-color: <?php __the_field( 'color_theme' ); ?>;" />
                    <?php the_field( 'sub_copy' ); ?>
                </div>
            </div>
        </div>
    </div>

    <?php
        if ( have_posts() ) :
            while ( have_posts() ) :
                the_post();
                the_content();
            endwhile;
        endif;
    ?>

</div>

<?php
rdmgumby_output_responsive_backgrounds();
get_footer();
