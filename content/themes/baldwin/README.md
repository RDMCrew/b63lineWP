# Baldwin Creative & Co.

WordPress site with custom theme built around [PageBuilder](https://siteorigin.com/page-builder/) for Baldwin Creative & Co. website. PageBuilder is a visual editor built around the WordPress widget ecosystem.

## Page Options

* The `Title` of the page will be used for the small top text in the hero section.
* `Main Copy` will be used as the large text in the hero section.
* `Sub Copy` will be used as the longer medium text in the hero section.
* `Color Theme` will be used to color shared elements on each of the pages. Things like separator lines, and the background color in the footer call to action. Note that the `Color Theme` set on the blog page will dictate the `Color Theme` for all blog-related pages. Colors can be defined with the `Colors` custom post type.
* `Hero Style` will define whether the hero section should take up the full browser window or a shorter version as is used on the Contact or sepcific Case Study pages.
* `Featured Image` will be used as the hero section background image.

## Home Sections

Home Sections will define what you see on the front page and in what order.

* `Is Contact Section` is a special flag for the contact section. This will dictate what content to display, but nothing else is pre-defined in this section.
* `Sub Copy` is optional, and will display an additional line of text colored based on the section's `Color Theme`.
* `Color Theme` is for each section, and will define the color theme to use for this section.
* `Track Icon` is the icon to display on the line path in the section. Note that `icon-*.svg` must exist in the theme images under `/assets/img/`. If you add a new section, and need a new icon, you'll need to create it and add it to the theme.
* `Track Position` will define how to position the track. Note that `Start` and `End` both have static positions. So in order for the track pieces to line up, the track following the `Start` piece must be set to `Left` and the track preceding the `End` piece must be set to `Right`. This effectively limits the number of home sections you can have to an even number.
* You can also define a background image or video, and whether or not to display a button.

## Menus

The mobile menu and floating bubble navigation menus are defined in WordPress. Mobile Menu is pretty straight-forward, but the Bubble Nav has one important gotcha. The `Navigation Label` here will also be used for the icon. So `icon-*.svg` and `icon-*-black.svg` must both exist in the theme images under `/assets/img/` in order to for everything to display properly. The `black` version will be shown when the item is not the current section on the home page.

## Custom Widgets

A number of custom widgets were built to work with PageBuilder. These all begin with `Baldwin` and can be used like any other widget. Here's a breakdown of the widgets, with information and modifiers.

#### PageBuilder Row

Start by adding a row, and from there widgets can be added to each row. Note that rows by default have a bottom margin of 30px. You can remove this margin by editing the row, and under Layout there's an option to set the bottom margin. In general, you'll set this to 0.

There are some custom modifiers you can add to the Row Classes, under Attributes. In particular these will be useful while building `Case Studies` post types.

* `baldwin-one-column-row` will put the content on the grid, for a one column layout.
* `baldwin-two-column-row` will put the content on the grid, for a two column layout.
* `float-image-right` and `float-image-left` is a modifier that will allow you to have text wrapped around an image. You would add the modifier to a row, then place an image widget first, then a text widget second. This will give you the wrapped text, as in the `Presence Medical Group` case study.

#### General

* Most text area boxes you will want to also check the `Automatically add paragraphs` option. This will get your text properly formatted.
* An element with a background image can be set to a fixed background style. You will add `fixed-background` to the Row Class, under Attributes.
* `dark typography` or `light typography` can be used in a Row or Cell class, under Attributes, to dictate the color of the typography. `dark` and `light` refer to the color of the background.

#### Baldwin Call-to-Action Box

This is a full-width area with text and a button on the grid. You can style the text here in a couple ways. You can add a line break `<br/>` to drop the text to the next line. You can also add a `<span></span>` tag. Any text within this tag will be highlighted based on the `Color Theme` of the page.

Button Link can either be a URL to a page, or you can call out a Business Options field by using the checkbox. For instance, you might want to link to a google map URL defined in the Business Options. In this case, you would check the box, and use `dayton_office_map` as the Button Link.

#### Baldwin Case Study Box

Used in the Case Study listing. Each box represents a different case study, and they can and should all be placed in the same row.

* Tags should be a list of job types. For instance, `Print / Digital / Messaging / Iconography`.
* Link / Slug should be the slug of the Case Study page you want to link to.
* Image is the background image to be used for the Case Study.
* Logo is the logo image to use for the Case Study.

#### Baldwin Collaborators Box

Designed to display the Collaborators custom post type. All published Collaborators will be displayed, with a link to the collaborator's site if there is one.

#### Baldwin Infographic Box

Displays a full-width infographic. The image to use for the graphic is defined here. For mobile, it gets a little trickier, and this is why this custom widget was built. Mobile breaks down into a special list of items and hides the actual graphic. Each item in the list - one per row - can have special formatting. If no formatting is included in the row, the text will appear in italics. If the format of `[number][color]' is used in the front of the line, followed by a space, the line will be formatted with a colored, numbered icon, followed by the text. For instance, here is an example for this list:

```
Project Briefing >>
[1][#ad9600] Understanding Business Goals
[2][#ad024a] Conduct Research
[3][#8160ac] Develop Strategy
[4][#e8a713] Design & Build
[5][#a86401] Launch
[6][#282828] Analyze & Optimize
Repeat Process >>
```

#### Baldwin Quote Box

A full sized box with a background color with text placed in the grid. Set the background color in the row element, under Design. You could also set a background image, set to cover in Background Image Display.

#### Baldwin Service Box

Designed to display the various Services. The `Bullets` are a straight list of items to display with no special formatting - one item per line. Note that in order for the icon to correctly display, the title must match the name of the icon, and `icon-*.svg` must exist in the theme images under `/assets/img/`.

#### Baldwin Staff Members Box

Designed to display the Staff Members custom post type. All published Staff Members will be displayed, with a slide-in detail box.

#### Baldwin Text Box

Super simple - a text area on the grid. The container row can be given a background color, or white would be the default.

This can also be used to inject a gravity forms form, using a shortcode. Like this `[gravityform id="1" title="false" description="false" ajax="true"]`.

#### Baldwin Text Box with Background

You will want this to be the only widget in a PageBuilder row. Titled text is pushed to the right, backed by a gradient that is colored based on the page's `Color Theme`. The full row can be backed by an image, defined in the row settings, under Design. Be sure to set the Background Image Display to `Cover` as well. You can also fix the background by used the `fixed-background` class on the row, under Attributes.

#### Baldwin Titled Text Box

Simple text area with a title and a line separator.

#### Baldwin Twitter Feed with Text Box

A 2-column widget. The left side will contain text with a title. The right side will display the Twitter feed defined in the widget. The background color defined in the row will color the left side text, which the right side will have the standard dark gray background color.

#### Baldwin Video Embed Box

Simple video embedder. URL should be the embed URL of the video you want to show.
